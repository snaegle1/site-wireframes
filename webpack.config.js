const Encore = require('@symfony/webpack-encore');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const GoogleFontsPlugin = require("google-fonts-webpack-plugin");
const WebpackConfig = require('@symfony/webpack-encore/lib/WebpackConfig');


// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
  // directory where compiled assets will be stored
  .setOutputPath('dist/build/')
  .cleanupOutputBeforeBuild()
  // public path used by the web server to access the output path
  .setPublicPath('/build')
  // only needed for CDN's or sub-directory deploy
  //.setManifestKeyPrefix('build/')

  .copyFiles({
    from: './assets/images',
    from: './src/'
  })
  
  /*
   * ENTRY CONFIG
   *
   * Add 1 entry for each "page" of your app
   * (including one that's included on every page - e.g. "app")
   *
   * Each entry will result in one JavaScript file (e.g. app.js)
   * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
   */
  .addEntry('app', './assets/js/app.js')
  //.addEntry('page1', './assets/page1.js')
  //.addEntry('page2', './assets/page2.js')

  // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
  //.splitEntryChunks()

  // will require an extra script tag for runtime.js
  // but, you probably want this, unless you're building a single-page app
  .disableSingleRuntimeChunk()

  /*
   * FEATURE CONFIG
   *
   * Enable & configure other features below. For a full
   * list of features, see:
   * https://symfony.com/doc/current/frontend.html#adding-more-features
   */
  .enableSourceMaps(!Encore.isProduction)
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  // enables hashed filenames (e.g. app.abc123.css)
  // .enableVersioning(Encore.isProduction())

  // enables @babel/preset-env polyfills
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = 'usage';
    config.corejs = 3;
  })

  .enableSassLoader()
  .enablePostCssLoader()
  // uncomment if you're having problems with a jQuery plugin
  .autoProvidejQuery()
//.addPlugin(new StyleLintPlugin({
// files: './assets/scss/**/*.scss',
//  fix: true,
//}));

if (!Encore.isProduction()) {
  Encore.enableEslintLoader((options) => {
    options.fix = true;
  });
}
module.exports = {
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader'
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  }
}
module.exports = Encore.getWebpackConfig();
