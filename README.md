# Testing Wireframes
This repo is designed to cater for rapid prototyping with bootstrap for example.

### To start run
`nvm use
npm install yarn
./node_modules/yarn/bin/yarn install
./node_modules/yarn/bin/yarn run encore dev'

### deploy
After your set up run php -S localhost:8000 -t dist/build

### change
Change the content of Resources/Private/Content.html to your liking, run
`./yarn run encore dev watch'
