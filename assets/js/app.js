// eslint-disable-next-line
import 'bootstrap/dist/js/bootstrap';

// eslint-disable-next-line
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap-icons/font/bootstrap-icons.css');
require('../scss/app.scss');

$(document).ready(() => {
  document.querySelectorAll('.card-columns .card').forEach((item) => {
    item.addEventListener('mouseenter', (event) => {
      event.target.querySelector('.card-title').classList.add('hidden');
      event.target.querySelector('p').classList.add('shown');
    });
    item.addEventListener('mouseleave', (event) => {
      event.target.querySelector('.card-title').classList.remove('hidden');
      event.target.querySelector('p').classList.remove('shown');
    });
  });
});
