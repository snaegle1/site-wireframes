<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Wireframe test</title>
  <link rel="stylesheet" type="text/css" href="app.css" media="all">
  <script type="text/javascript" src="app.js" media="all"></script>
 </head>
 <body>
 <?php include('./Resources/Private/Inhalt.html'); ?>
 </body>
</html>

